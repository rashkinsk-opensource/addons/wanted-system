AddCSLuaFile("cl_init.lua")
AddCSLuaFile("shared.lua")
include("shared.lua")


function ENT:Initialize()
	self:SetModel("models/player/zubenko.mdl")
	self:SetHullType( HULL_HUMAN )
	self:SetHullSizeNormal()
	self:SetSolid( SOLID_BBOX )
	self:CapabilitiesAdd( CAP_ANIMATEDFACE )
	self:CapabilitiesAdd( CAP_TURN_HEAD )
	self:DropToFloor()
	self:SetMoveType( MOVETYPE_NONE )
	self:SetCollisionGroup( COLLISION_GROUP_PLAYER )
	self:SetUseType( SIMPLE_USE )

	local phys = self:GetPhysicsObject()
	if IsValid(phys) then
		phys:EnableMotion(false)
	end
	self:SetExpression( "scenes/streetwar/sniper/ba_nag_grenade0"..math.random( 1, 5 )..".vcd" )
end

--[[ function BuyUnwanted(answer, pl, target)
	if tobool(answer) then
		Wanted_SetStars(pl, 0)
		pl:CleanWantedReasons()
		pl:unWanted()
		target:unWanted()
	end
end ]]

function ENT:PlayerUse(pl)
	if (pl:getDarkRPVar("money") < WANTED_CONFIG.Unwanted_Price*GetWantedLevel(pl)) then
		DarkRP.notify(pl, 1, 10, "У вас не хватает денег! Цена: "..WANTED_CONFIG.Unwanted_Price*GetWantedLevel(pl).." рублей")
		return false
	end
	if (GetWantedLevel(pl) >= 1 and !pl:isCP()) then
		--DarkRP.createQuestion("Вы готовы заплатить \n"..WANTED_CONFIG.Unwanted_Price.." руб. за снятие розыска?", 100500, pl, 40, BuyUnwanted, pl)
		local wantedlevel = GetWantedLevel(pl)
		Wanted_SetStars(pl, 0)
		pl:CleanWantedReasons()
		pl:unWanted()
		pl:SetNWBool("Wanted_Visible", false)
		pl:addMoney(-WANTED_CONFIG.Unwanted_Price*wantedlevel)
		DarkRP.notify(pl, 0, 10, "Вы успешно купили новые документы. Розыск снят. Цена: "..WANTED_CONFIG.Unwanted_Price*wantedlevel.." рублей")
	elseif GetWantedLevel(pl) < 1 and !pl:isCP() then
		DarkRP.notify(pl, 1, 10, "Во-первых, ты не в розыске. Во-вторых, я таким не занимаюсь вообще...")
	else
		DarkRP.notify(pl, 1, 10, "У нас обед. Приходите позже...")
	end
end

function ENT:AcceptInput(name, activator, pl, data)
	if name == "Use" then
		self:PlayerUse(pl)
	end
end

