--AddCSLuaFile( "autorun/client/cl_init.lua" )
 include( "autorun/wanted_config.lua" )

print("Autowanted loaded - shared")

local Levels = {
WANTED_CONFIG.star1,
WANTED_CONFIG.star2,
WANTED_CONFIG.star3,
WANTED_CONFIG.star4,
WANTED_CONFIG.star5,
}

function Wanted_AddStars(ply, amount, maxlevel)
	if maxlevel > 5 or maxlevel < 1 then
		ply:SetNWInt("Wanted_Stars", Wanted_GetStars(ply) + amount)
		--ply:SetRData("Wanted_Stars", Wanted_GetStars(ply) + amount)
	else
		if (Wanted_GetStars(ply) + amount) < Levels[maxlevel+1] then
			ply:SetNWInt("Wanted_Stars", Wanted_GetStars(ply) + amount)
			--ply:SetRData("Wanted_Stars", Wanted_GetStars(ply) + amount)
		end
	end
	
end

function Wanted_RemoveStars(ply, amount)
	ply:SetNWInt("Wanted_Stars", Wanted_GetStars(ply) - amount)
	--ply:SetRData("Wanted_Stars", Wanted_GetStars(ply) - amount)
end

function Wanted_SetStars(ply, amount)
	ply:SetNWInt("Wanted_Stars", amount)
	--ply:SetRData("Wanted_Stars", amount)
end


function Wanted_GetStars(ply)
	return ply:GetNWInt("Wanted_Stars", 0)
end

local function ExecReportLockpick(answer, ent, ply, target)
    if not tobool(answer) then return end
	Wanted_AddStars(ply, WANTED_CONFIG.Penalty_Lockpick, 2)
end

hook.Add("EntityTakeDamage", "WantedDMG", function( target, dmginfo )
	if target:IsPlayer() and target:isCP() then
		local attacker = dmginfo:GetAttacker()
		local damage = dmginfo:GetDamage()
		if !attacker:isCP() then	
			if dmginfo:IsBulletDamage() then
				Wanted_AddStars(attacker, WANTED_CONFIG.Damage_Police_Bullet, 3)
			else
				Wanted_AddStars(attacker, WANTED_CONFIG.Damage_Police, 2)
			end
		end
	end
end)

hook.Add("PlayerDeath", "WantedDeath", function( victim, inflictor, attacker )
    if not IsValid(attacker) or not attacker:IsPlayer() then
        return
    end
    
    
	if victim:isCP() and !attacker:isCP() then
		if !attacker:isWanted() then
			attacker:wanted(target, "Убийство сотрудника", 1000)
			if GetWantedLevel(attacker) < 3 then
				Wanted_SetStars(attacker, WANTED_CONFIG.star3)
			else
				Wanted_AddStars(attacker, WANTED_CONFIG.Murder_Police, 0)
			end
		else
			attacker:setDarkRPVar("wanted", true)
			attacker:setDarkRPVar("wantedReason", "Убийство сотрудника")
			if GetWantedLevel(attacker) < 3 then
				Wanted_SetStars(attacker, WANTED_CONFIG.star3)
			else
				Wanted_AddStars(attacker, WANTED_CONFIG.Murder_Police, 0)
			end
		end
		Target:SetNWBool("Wanted_Visible", true)
		if !timer.Exists("WantedShow"..Target:EntIndex()) then
			timer.Create("WantedShow"..Target:EntIndex(), 30, 1, function() 
				Target:SetNWBool("Wanted_Visible", false)
				DarkRP.notify(Target, 0, 10, "Теперь полиция не видит ваш розыск, пока не проверит документы.")	
			end)
		end
	elseif attacker:isCP() and !victim:isCP() and GetWantedLevel(victim) >= WANTED_CONFIG.Police_Kill_Jail then
		--victim:arrest(GetWantedLevel(victim) * WANTED_CONFIG.Jail_time_star, attacker)
		victim:RHC_Arrest(GetWantedLevel(victim) * WANTED_CONFIG.Jail_time_star, attacker, "Ранен при задержании", false)
		Wanted_SetStars(victim, 0)
		victim:SetNWBool("Wanted_Visible", false)
	end
end)

hook.Add("playerArrested", "Wanted_Arrest", function( criminal, time, actor )
	criminal:SetNWBool("Wanted_Visible", false)
	Wanted_SetStars(criminal, 0)
end)

local function spawn( ply )
	--ply:SetNWInt("Wanted_Stars", ply:GetRData("Wanted_Stars", 0))
end
hook.Add( "PlayerInitialSpawn", "Dubr_AutoWanted_Join", spawn )

hook.Add("playerWanted", "Wanted_AddCommand", function(criminal, actor, reason)
	Wanted_AddStars(criminal, 2, 6)
end)

hook.Add("playerUnWanted", "Wanted_RemoveCommand", function(excriminal, actor)
	Wanted_RemoveStars(excriminal, 2)
end)