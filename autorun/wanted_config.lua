﻿AddCSLuaFile()
print("Dubrovski Wanted System (special for Rashkinsk) - Config loaded") -- Можно убрать при заливе на основу. Нужно чисто для теста

WANTED_CONFIG = {}

/*
	█▀▀▄ █░░█ █▀▀▄ █▀▀█ █▀▀█ ▀█░█▀ █▀▀ █░█ ░▀░   █░░░█ █▀▀█ █▀▀▄ ▀▀█▀▀ █▀▀ █▀▀▄   █▀▀ █░░█ █▀▀ ▀▀█▀▀ █▀▀ █▀▄▀█
	█░░█ █░░█ █▀▀▄ █▄▄▀ █░░█ ░█▄█░ ▀▀█ █▀▄ ▀█▀   █▄█▄█ █▄▄█ █░░█ ░░█░░ █▀▀ █░░█   ▀▀█ █▄▄█ ▀▀█ ░░█░░ █▀▀ █░▀░█
	▀▀▀░ ░▀▀▀ ▀▀▀░ ▀░▀▀ ▀▀▀▀ ░░▀░░ ▀▀▀ ▀░▀ ▀▀▀   ░▀░▀░ ▀░░▀ ▀░░▀ ░░▀░░ ▀▀▀ ▀▀▀░   ▀▀▀ ▄▄▄█ ▀▀▀ ░░▀░░ ▀▀▀ ▀░░░▀
*/
-- Сколько нужно набрать Очков Нарушений чтобы получить 1 звезду
WANTED_CONFIG.star1 = 0
-- Сколько нужно набрать Очков Нарушений чтобы получить 2 звезды
WANTED_CONFIG.star2 = 5
-- Сколько нужно набрать Очков Нарушений чтобы получить 3 звезды
WANTED_CONFIG.star3 = 10
-- Сколько нужно набрать Очков Нарушений чтобы получить 4 звезды
WANTED_CONFIG.star4 = 50
-- Сколько нужно набрать Очков Нарушений чтобы получить 5 звезд
WANTED_CONFIG.star5 = 100

-- =======================================
-- Распределение очков по типам нарушений:
-- =======================================

-- За нанесение урона игроку
WANTED_CONFIG.Damage_Citizen = 1.5
-- За убийство игрока
WANTED_CONFIG.Murder_Citizen = 3

-- За нанесение урона полиции (кулаками)
WANTED_CONFIG.Damage_Police = 2
-- За нанесение урона полиции (огнестрельное оружие)
WANTED_CONFIG.Damage_Police_Bullet = 2.5
-- За убийство сотрудника полиции
WANTED_CONFIG.Murder_Police = 10

-- За взлом чего-либо
WANTED_CONFIG.Penalty_Lockpick = 5

-- За нарушение ПДД
WANTED_CONFIG.Penalty_Road = 1

-- За ношение нелегального оружия
WANTED_CONFIG.Illegal_weapon = 1.5

-- За ограбление банка
WANTED_CONFIG.Bank_Robbery = 20

-- За продажу мета
WANTED_CONFIG.Meth_selling = 2.5

-- ======================================
-- Расстояние необходимое для запала взлома
WANTED_CONFIG.Lockpick_Distance = 5000

-- Сколько стоит снять розыск у NPC? (За одну звезду)
WANTED_CONFIG.Unwanted_Price = 2000

-- При каком минимальном розыске человек при убийстве копами будет сажаться в тюрьму а не возвращаться на спавн без снятия розыска?
WANTED_CONFIG.Police_Kill_Jail = 3

-- Столько секунд будет сидеть человек за наличие 1 звезды (умножается кол-во звёзд * число ниже)
WANTED_CONFIG.Jail_time_star = 120

-- Сколько секунд показывается розыск человека, если у того 1 звезда
WANTED_CONFIG.ShowWanted_1 = 120
-- Сколько секунд показывается розыск человека, если у того 2 звезды
WANTED_CONFIG.ShowWanted_2 = 300


/*
	█▀▀ █▀▀▄ █▀▀▄   █▀▀█ █▀▀   █▀▀ █▀▀█ █▀▀▄ █▀▀ ░▀░ █▀▀▀
	█▀▀ █░░█ █░░█   █░░█ █▀▀   █░░ █░░█ █░░█ █▀▀ ▀█▀ █░▀█
	▀▀▀ ▀░░▀ ▀▀▀░   ▀▀▀▀ ▀░░   ▀▀▀ ▀▀▀▀ ▀░░▀ ▀░░ ▀▀▀ ▀▀▀▀
*/

function GetWantedLevel(ply)
	local wantedpoints = ply:GetNWInt("Wanted_Stars", 0)
	if wantedpoints <= WANTED_CONFIG.star1 then 
		return 0
	elseif wantedpoints > WANTED_CONFIG.star1 and wantedpoints < WANTED_CONFIG.star2 then 
		return 1
	elseif wantedpoints >= WANTED_CONFIG.star2 and wantedpoints < WANTED_CONFIG.star3 then 
		return 2
	elseif wantedpoints >= WANTED_CONFIG.star3 and wantedpoints < WANTED_CONFIG.star4 then 
		return 3
	elseif wantedpoints >= WANTED_CONFIG.star4 and wantedpoints < WANTED_CONFIG.star5 then 
		return 4
	elseif wantedpoints >= WANTED_CONFIG.star5 then 
		return 5
	end
end

function GetWantedVisible(ply)
	local isvisible = ply:GetNWBool("Wanted_Visible", false)
	local wantedlevel = GetWantedLevel(ply)
	
	if wantedlevel <= 3 then
		if isvisible then return true end
	elseif wantedlevel > 3 then
		return true
	end
	
end
