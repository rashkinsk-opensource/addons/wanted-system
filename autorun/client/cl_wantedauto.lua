﻿--AddCSLuaFile()
include( "autorun/wanted_config.lua" )
AddCSLuaFile()

local starmaterial = Material("icon16/star.png")
local function DrawInfoWanted(pos, txt, clr)
	pos = pos:ToScreen()
	draw.TextShadow( {
		text = "Особо опасен!",
		pos = { pos.x + 1, pos.y - 1},
		font = "RashkinskFutura9",
		color = clr,
		xalign = TEXT_ALIGN_CENTER,
		yalign = TEXT_ALIGN_CENTER,
	}, 2, 255 )
	draw.TextShadow( {
		text = txt,
		pos = { pos.x + 1, pos.y + 20},
		font = "RashkinskFutura9",
		color = clr,
		xalign = TEXT_ALIGN_CENTER,
		yalign = TEXT_ALIGN_CENTER,
	}, 2, 255 )
	surface.SetDrawColor( 255, 255, 0, 255 )
	surface.SetMaterial( starmaterial ) -- If you use Material, cache it!
	surface.DrawTexturedRect( pos.x-8, pos.y-35, 16, 16 )
end

hook.Add( "HUDPaint", "WantedShow_HUD", function()

	if LocalPlayer():isCP() then
		for k,v in pairs(player.GetAll()) do
			if GetWantedLevel(v) >= 5 then 
				if LocalPlayer():GetPos():Distance(v:GetPos()) > 350 then
					DrawInfoWanted(Vector(v:GetPos().x, v:GetPos().y, v:GetPos().z + 75), v:Nick().." ("..math.Round(v:GetPos():Distance(LocalPlayer():GetPos())/100).." м)", Color(200,0,0))
				end
			end
		end
	end
	
end)