
/*
General config
*/

print("autowanted config 2 loaded")

AUTOWANTED_MAXRANGE = 2000	// Maximum range a police can "see" a crime
AUTOWANTED_AIMLIMIT = 0.5		// Basically a variable that lets you change how much a policeman can see. Value of 1 means the
//CP needs to look exactly at the position of the crime. Value of -1 means he can look even in the opposite direction and still
//technically "see" the crime. 0.5 means a FOV of 90, which should be set as default. If you feel that you saw a crime, but it didn't
//get noticed, try lowering this value.
AUTOWANTED_BLOCKEDCHECK = true // Enables/Disables the walls check. If this is disabled, the police can wanted people through walls.

//Return true if this player should be allowed to set people as wanted.
//This is set as "return ply:isCP()" by default to only let police be able to do it.
//Setting this to "return true" will make anyone be able to set people as wanted, and because of that, be careful since the window
//can get quite annoying if you're a gangster and your friends are doing crimes all the time.
function playerCanReportCrime(ply)
	return ply:isCP()-- or ply:getJobTable().category == "Вооруженные Силы" or ply:getJobTable().category == "Министерство Юстиции" or ply:getJobTable().category == "Власть" or ply:getJobTable().category == "Мирные"
end

/*
Reasons to check for. Set any of these to false instead of true to disable it.
*/
AUTOWANTED_REASONS = {}
AUTOWANTED_REASONS["Contraband"] = true		//Put people as wanted if they're holding any item marked as contraband.
AUTOWANTED_REASONS["Murder"] = true				//Put people as wanted if they murdered someone
AUTOWANTED_REASONS["Harm"] = true					//Put people as wanted if they harmed (but not killed) someone
AUTOWANTED_REASONS["Unauthguns"] = true		//Put people as wanted if they're holding a weapon they're not authorized to use.
//This only works if GAMEMODE.Config.license is set to true. Make sure that weapons such as keys and pocket is added to
//GAMEMODE.NoLicense table, or they'll get detected as Unauthorized weapons.
AUTOWANTED_REASONS["Jailbailing"] = true	//Put people as wanted if a non-CP unarrest someones.
AUTOWANTED_REASONS["ArrestOnSight"] = true	//Put people as wanted if they're having an AoS job.
AUTOWANTED_REASONS["IllegalActivity"] = true	//Wants people for lockpick, keypad cracking, pickpocketing, ATM hacking.


/*
Illegal Activity
*/
function playerCommitingIllegalActivity(ply)
	local wep = ply:GetActiveWeapon()
	if not IsValid(wep) then return false end
	
	--if (wep.IsLockPicking) or (ply.IsLockPicking and ply:IsLockPicking()) or (wep.IsCracking) or (wep.GetPickpocketing and wep:GetPickpocketing()) or wep:GetIsLockpicking() then return true end
	if (wep.IsLockPicking) or (ply.IsLockPicking and ply:IsLockPicking()) or (wep.IsCracking) then return true end
	
	//ARitz Cracker's ATM mod
	for k,v in pairs(ents.FindByClass("sent_arc_atmhack")) do
		if v.Hacker == ply then
			local atm = v:GetParent()
			if IsValid(atm) and atm.IsAFuckingATM and ply:GetPos():DistToSqr(atm:GetPos()) < (100*100) then
				return true
			end
		end
	end
	
	return false
end

/*
ArrestOnSight
*/
local arrestonsightjobs = {}
//arrestonsightjobs[TEAM_GUN] = true // Example. Will make the gundealer be wanted as soon as a police sees one.

function playerHasIllegalJob(ply)
	return arrestonsightjobs[ply:Team()]
end

/*
Ignorelist

Make this return false if you don't want the player to be autowanted. Put for example "return not ply:isCP()" to prevent all cops from being
wanted (by autowanted)

Default: "return true" will make everybody eligible for autowanted.
*/
function playerCanBeAutowanted(ply)
	if !ply:isCP() and ply:getJobTable().category != "Админские" and ply:getJobTable().category != "Животные" and ply:getJobTable().category != "Вооруженные Силы" and ply:getJobTable().category != "Министерство Юстиции" and ply:getJobTable().category != "Власть" then
		return true
	end
end

/*
Unauthorized weapons
*/
local allowedteams = {}
allowedteams[TEAM_GUN] = true // Gundealers can hold guns without being criminals.

function playerAllowedToHoldGuns(ply)
	return ply:isCP() or allowedteams[ply:Team()] //If the player is a police, or he exists in one of the allowedteams, he can carry guns.
end

//List of weapons thats legal.
aw_legalweapons = {}
aw_legalweapons["gmod_tool"] = true
aw_legalweapons["weapon_keypadchecker"] = true
aw_legalweapons["vc_wrench"] = true
aw_legalweapons["vc_jerrycan"] = true
aw_legalweapons["vc_spikestrip_wep"] = true
aw_legalweapons["laserpointer"] = true
aw_legalweapons["remotecontroller"] = true
aw_legalweapons["idcard"] = true
aw_legalweapons["pickpocket"] = true
aw_legalweapons["keys"] = true
aw_legalweapons["pocket"] = true
aw_legalweapons["driving_license"] = true
aw_legalweapons["firearms_license"] = true
aw_legalweapons["weapon_physcannon"] = true
aw_legalweapons["gmod_camera"] = true
aw_legalweapons["weapon_physgun"] = true
aw_legalweapons["weapon_r_restrained"] = true
aw_legalweapons["tbfy_surrendered"] = true
aw_legalweapons["weapon_r_cuffed"] = true
aw_legalweapons["collections_bag"] = true
aw_legalweapons["weapon_fists"] = true
aw_legalweapons["weapon_arc_atmcard"] = true
aw_legalweapons["itemstore_pickup"] = true
aw_legalweapons["weapon_checker"] = true
aw_legalweapons["driving_license_checker"] = true
aw_legalweapons["fine_list"] = true
aw_legalweapons["weapon_r_handcuffs"] = true
aw_legalweapons["door_ram"] = true
aw_legalweapons["med_kit"] = true
aw_legalweapons["stunstick"] = true
aw_legalweapons["arrest_stick"] = true
aw_legalweapons["unarrest_stick"] = true
aw_legalweapons["weaponchecker"] = true
aw_legalweapons["arrest_stick"] = true
aw_legalweapons["weapon_bugbait"] = true
aw_legalweapons["door_ram"] = true
aw_legalweapons["gmod_camera"] = true
aw_legalweapons["gmod_tool"] = true
aw_legalweapons["keys"] = true
aw_legalweapons["lockpick"] = true
aw_legalweapons["med_kit"] = true
aw_legalweapons["pocket"] = true
aw_legalweapons["stunstick"] = true
aw_legalweapons["unarrest_stick"] = true
aw_legalweapons["weapon_keypadchecker"] = true
aw_legalweapons["weapon_physcannon"] = true
aw_legalweapons["weapon_physgun"] = true
aw_legalweapons["weaponchecker"] = true
aw_legalweapons["weapon_checker"] = true
aw_legalweapons["weapon_shield"] = true
aw_legalweapons["climb_swep2"] = true
aw_legalweapons["weapon_hookah"] = true
aw_legalweapons["weapon_gpee"] = true
aw_legalweapons["weapon_cuff_police"] = true
aw_legalweapons["weapon_cuff_tactical"] = true
aw_legalweapons["riot_shield"] = true
aw_legalweapons["selfportrait_camera"] = true
aw_legalweapons["lods"] = true
aw_legalweapons["breachingcharge"] = true
aw_legalweapons["weapon_rape"] = true
aw_legalweapons["slappers"] = true
aw_legalweapons["itemstore_pickup"] = true
aw_legalweapons["sieghail"] = true
aw_legalweapons["weapon_pet"] = true
aw_legalweapons["weapon_tonfa"] = true
aw_legalweapons["nova_mechanic"] = true
aw_legalweapons["nova_speedgun"] = true
aw_legalweapons["nova_spikestrip_deployer"] = true
aw_legalweapons["nova_mechanic"] = true
aw_legalweapons["nova_speedgun"] = true

aw_legalweapons["weapon_extinguisher_infinite"] = true
aw_legalweapons["tfa_nmrih_kknife"] = true
aw_legalweapons["molochkov_geometrichkasuka"] = true
aw_legalweapons["dab"] = true
aw_legalweapons["middlefinger"] = true
aw_legalweapons["facepunch"] = true
aw_legalweapons["vc_jerrycan"] = true
aw_legalweapons["vc_wrench"] = true
aw_legalweapons["rashkinsk_vosstanovlenie"] = true
aw_legalweapons["slappers"] = true
aw_legalweapons["weapon_bomj_molochkov"] = true
aw_legalweapons["salute"] = true
aw_legalweapons["weapon_russianbat"] = true
aw_legalweapons["weapon_huntingbow"] = true
aw_legalweapons["weapon_magnitolastealer"] = true
aw_legalweapons["weapon_ciga"] = true
aw_legalweapons["rashkinsk_vorovskaya"] = true
aw_legalweapons["weapon_extinguisher"] = true

aw_legalweapons["keypad_cracker"] = true
aw_legalweapons["dab"] = true
aw_legalweapons["weapon_medkit"] = true
aw_legalweapons["weapon_cat"] = true
aw_legalweapons["tfa_nmrih_bat"] = true
aw_legalweapons["tfa_bo4_mp40"] = true
aw_legalweapons["weapon_biggiebong"] = true
aw_legalweapons["weapon_ciga"] = true
aw_legalweapons["weapon_vilka"] = true
aw_legalweapons["surrender"] = true
aw_legalweapons["weapon_huntingbow"] = true
aw_legalweapons["flip"] = true
aw_legalweapons["frontflip"] = true
aw_legalweapons["weapon_ciga_blat"] = true
aw_legalweapons["weapon_ciga_cheap"] = true
aw_legalweapons["weapon_banner0_peplix"] = true
aw_legalweapons["weapon_banner0_ebu"] = true
aw_legalweapons["weapon_banner0"] = true
aw_legalweapons["swep_radiodevice"] = true
aw_legalweapons["rashkinsk_vorovskaya"] = true
aw_legalweapons["tfa_cso_elvenranger"] = true

/*
Contraband checking

ADVANCED USERS
This function is used to check if an entity is illegal. Add specific classes to the contraclasses table. Or do as I did and
make a string.find if you want regex.
*/
local contraclasses = {}
contraclasses["drug"] = true
contraclasses["drug_lab"] = true
function isContraband(ent)
	if string.find(ent:GetClass(), "money.*print") then return true end // Should get all the moneyprinter cases.
	if contraclasses[ent:GetClass()] then return true end
	
	return false
end
