															 
local info
local barstart
local font = "RashkinskFutura3"



local screenwidth = ScrW()
local screenheight = ScrH()

local scrw1 = 1/1600*screenwidth
local scrh1 = 1/900*screenheight

local text
local textw
local text2 = "Нажмите F2 чтобы сообщить..."
local text2w = 163*scrw1
local text3 = "Передаём данные..."
local text3w = 131*scrw1

local canaccept = false
net.Receive("startwantedtimer", function()
	local criminal = net.ReadEntity()
	local reason = net.ReadString()
	
	barstart = nil
	info = {cr = criminal, re = reason}
	
	text = string.format("%s совершил преступление: %q.",info.cr:Nick(),info.re)
	surface.SetFont(font)
	textw = surface.GetTextSize(text)
	
	canaccept = true
	
	timer.Create("AutoWanted", AUTOWANTED_REACTTIME, 1, function()
		info = nil
		canaccept = false
	end)
end)

net.Receive("abortwantedtimer", function()
	info = nil
	timer.Destroy("AutoWanted")
	canaccept = false
end)

function AcceptAutoWanted()
	canaccept = false
	timer.Destroy("AutoWanted")
	barstart = CurTime()
	RunConsoleCommand("_acceptautowanted")
	
	--[[ surface.PlaySound("npc/metropolice/vo/reportsightingsaccomplices.wav")
	timer.Simple(2, function()
		if not LocalPlayer():Alive() then return end
		surface.PlaySound("npc/metropolice/vo/quick.wav")
		timer.Simple(1, function()
			if not LocalPlayer():Alive() then return end
			surface.PlaySound("npc/metropolice/vo/prosecute.wav")
			timer.Simple(1.5, function()
				if not LocalPlayer():Alive() then return end
				surface.PlaySound("npc/metropolice/vo/rodgerthat.wav")
			end)
		end)
	end) ]]
end

hook.Add("Think", "AutoWanted", function()
	if gui.IsGameUIVisible() then return end
	if LocalPlayer():IsTyping() then return end
	
	if input.IsKeyDown(KEY_F2) then
		if canaccept then
			AcceptAutoWanted()
		end
	end
end)


//hue, sat, val
local c1 = Color(50, 50, 50, 100)
local c2 = Color(255,255,255)
local c3 = Rashkinsk.UIColors.Red

local w,h = ScrW(), ScrH()
hook.Add("HUDPaint", "AutoWanted", function()
	if not info then return end
	if not IsValid(info.cr) then info = nil return end //Criminal disconnected.
	if not LocalPlayer():Alive() then info = nil return end //If the guy died, he shouldn't be able to report the crime.
	
	local boxw = textw + 8*scrw1
	local boxh = (4+20+4+20+2+6+2)*scrh1
	
	draw.RoundedBox(4, w - boxw - 40*scrw1, h/2, boxw, boxh, c1)
	
	surface.SetTextColor(c2)
	surface.SetFont(font)
	
	surface.SetTextPos(w - boxw - 36*scrw1, h/2 + 4*scrh1)
	surface.DrawText(text)
	
	draw.RoundedBox(0, w - boxw - 40*scrw1, h/2 + 28*scrh1, boxw, 6*scrh1, c3)
	if barstart then
		local barw = math.Clamp(((CurTime() - barstart) / AUTOWANTED_RESPONSETIME) * (boxw), 4*scrh1, boxw-8*scrw1)
		draw.RoundedBox(0, w - boxw - 40*scrw1, h/2 + 28*scrh1, barw, 6*scrh1, c2)
		
		if not LocalPlayer():Alive() or CurTime()>barstart+AUTOWANTED_RESPONSETIME then
			info = nil
		end
		
		surface.SetTextPos(w - boxw/2 - 40*scrw1 - surface.GetTextSize(text3)/2, h/2 + 36*scrh1)
		surface.DrawText(text3)
	else
		surface.SetTextPos(w - boxw/2 - 40*scrw1 - surface.GetTextSize(text2)/2, h/2 + 36*scrh1)
		surface.DrawText(text2)
	end
end)
