
/*
Returns true if the player can see this position
*/
local function plyCanSee( ply, pos )
	if ply:GetShootPos():Distance(pos) > AUTOWANTED_MAXRANGE then return false end
	if ply:GetAimVector():Dot((pos - ply:GetShootPos()):GetNormal()) < AUTOWANTED_AIMLIMIT then return false end
	
	if AUTOWANTED_BLOCKEDCHECK then
		local trdata = {}
			trdata.start = ply:GetShootPos()
			trdata.endpos = pos
			trdata.mask = CONTENTS_SOLID // only hit worldbrushes
			trdata.filter = ply
		local trres = util.TraceLine(trdata)
		if trres.Hit then return false end
	end
	
	return true
end

/*
Returns true,who from a list who's watching that pos.
*/
local function isListWatching( t, pos )
	local watchers = {}
	for k,v in pairs(t) do
		if plyCanSee( v, pos ) then table.insert(watchers, v) end
	end
	
	if #watchers > 0 then
		return true, watchers
	end
	
	return false, {}
end

/*
Returns true,who if a policeauthority is watching that position
*/
function isPoliceWatching( pos )
	local t = {}
	
	for k,v in pairs(player.GetAll()) do
		if playerCanReportCrime(v) then table.insert(t, v) end
	end
	
	return isListWatching(t, pos)
end


local meta = FindMetaTable("Player")

/*
Returns true if the player has this wanted reason
*/
function meta:HasWantedReason(reason)
	self.wantedreasons = self.wantedreasons or {}
	return table.HasValue(self.wantedreasons, reason)
end

/*
Clears the players wanted reasons
*/
function meta:CleanWantedReasons()
	self.wantedreasons = {}
	self.hurtppl = {}
	self.customwanted = nil
end

/*
Returns a pretty text for use with wanted reason
*/
local function getWantedReason(ply)
	local t = table.Copy(ply.wantedreasons)
	if ply.customwanted then
		table.insert(t, 1, ply.customwanted)
	end
	return table.concat(t, ", ")
end

/*
Removes a wanted reason
*/
/*
not used right now

function meta:RemoveWantedReason(reason)
	if not self.wantedreasons then return false end
	for k,v in pairs(self.wantedreasons) do
		if v == reason then
			table.remove(self.wantedreasons, k)
			return true
		end
	end
	return false
end
*/

/*
The function that really adds a wanted reason, the playermethod just tells the police that he can call him out
*/
local function addwantedreasonreal(criminal, reason, actor)
	actor.pollingwantedreason = nil
	
	if not IsValid(criminal) then return end
	
	local force = true
	if reason == "Нелегальное оружие" then
		force = false
	end
	if criminal:HasWantedReason(reason) and !force then return end
	
	//Support for if theres any other wanted reasons BEFORE we add our ones.
	//If the guy is already wanted for "Taking a piss", "Taking a piss" will get set as criminal.customwanted.
	if not criminal.customwanted and criminal:isWanted() and #criminal.wantedreasons == 0 then
		criminal.customwanted = criminal:getWantedReason()
	end
	
	if !criminal:HasWantedReason(reason) then
		table.insert(criminal.wantedreasons, reason)
	
		criminal:wanted(actor, getWantedReason(criminal))
	end
	
	if reason == "Взлом" then
		Wanted_AddStars(criminal, WANTED_CONFIG.Penalty_Lockpick, 3)
	elseif reason == "Ущерб здоровью" then
		Wanted_AddStars(criminal, WANTED_CONFIG.Damage_Citizen, 2)
	elseif reason == "Убийство" then
		Wanted_AddStars(criminal, WANTED_CONFIG.Murder_Citizen, 3)
	elseif reason == "Нелегальное оружие" then
		Wanted_AddStars(criminal, WANTED_CONFIG.Illegal_weapon, 2)
	elseif reason == "Нарушение ПДД" then
		Wanted_AddStars(criminal, WANTED_CONFIG.Penalty_Road, 2)
	elseif reason == "Ограбление банка" then
		Wanted_AddStars(criminal, WANTED_CONFIG.Bank_robbery, 6)
	elseif reason == "Продажа мета" then
		Wanted_AddStars(criminal, WANTED_CONFIG.Meth_selling, 4)
	end
	
	Target:SetNWBool("Wanted_Visible", true)
	if !timer.Exists("WantedShow"..Target:EntIndex()) then
		timer.Create("WantedShow"..Target:EntIndex(), 30, 1, function() 
			Target:SetNWBool("Wanted_Visible", false)
			DarkRP.notify(Target, 0, 10, "Теперь полиция не видит ваш розыск, пока не проверит документы.")	
		end)
	end
end


/*
Called when a player presses F2.
*/
concommand.Add("_acceptautowanted", function(ply,cmd,args)
	if not ply.pollingwantedreason then return end
	timer.Destroy(ply:EntIndex().."_autowanted")
	
	timer.Create(ply:EntIndex().."_autowanted2", AUTOWANTED_RESPONSETIME, 1, function()
		if IsValid(ply) and ply.pollingwantedreason then
			addwantedreasonreal(ply.pollingwantedreason.criminal, ply.pollingwantedreason.reason, ply)
		end
	end)
end)

util.AddNetworkString("startwantedtimer")
/*
The "fake" function for adding a wanted to the player.
*/

function meta:AddWantedReason(reason, actor, force)
	if self:HasWantedReason(reason) and force then 
		print(1)
		if actor.pollingwantedreason then return false end //Returning false here is so other cops can try make him get wanted if this guy is busy.
	
		actor.pollingwantedreason = {criminal = self, reason = reason}
		net.Start("startwantedtimer")
			net.WriteEntity(self)
			net.WriteString(reason)
		net.Send(actor)
		
		timer.Create(actor:EntIndex().."_autowanted", AUTOWANTED_REACTTIME, 1, function()
			if IsValid(actor) and actor.pollingwantedreason then
				actor.pollingwantedreason = nil
			end
		end)
		
		return true		
	
	elseif !self:HasWantedReason(reason) then
		print(2)
		if actor.pollingwantedreason then return false end //Returning false here is so other cops can try make him get wanted if this guy is busy.
	
		actor.pollingwantedreason = {criminal = self, reason = reason}
		net.Start("startwantedtimer")
			net.WriteEntity(self)
			net.WriteString(reason)
		net.Send(actor)
		
		timer.Create(actor:EntIndex().."_autowanted", AUTOWANTED_REACTTIME, 1, function()
			if IsValid(actor) and actor.pollingwantedreason then
				actor.pollingwantedreason = nil
			end
		end)
		
		return true	
	elseif self:HasWantedReason(reason) and !force then
		print(3)
		return false
	end
end

//If he gets unwanted, we have to clean our stuff too
hook.Add("playerUnWanted", "AutoWanted", function( ply, actor )
	ply:CleanWantedReasons()
end)

//Aswell if he gets arrested.
hook.Add("playerArrested", "AutoWanted", function( criminal, time, actor )
	criminal:CleanWantedReasons()
end)

util.AddNetworkString("abortwantedtimer")
local function AbortWanted(ply)
	net.Start("abortwantedtimer")
	net.Send(ply)
	ply.pollingwantedreason = nil
	timer.Destroy(ply:EntIndex().."_autowanted")
	timer.Destroy(ply:EntIndex().."_autowanted2")
end

hook.Add("PlayerDisconnected", "AutoWanted", function(ply)
	for k,v in pairs(player.GetAll()) do
		if v.pollingwantedreason then
			local crim = v.pollingwantedreason.criminal
			if crim == ply then
				//ABORT
				AbortWanted(v)
				GAMEMODE:notify(v, 0, 5, "The criminal disconnected before you could report him!")
			end
		end
	end
	
	if ply.pollingwantedreason then
		AbortWanted(ply)
	end
end)

local function SetPlayerIgnore(ply)
	timer.Create("autowantedignoretimer"..ply:UserID(), 5, 1, function()
		if IsValid(ply) then
			ply.ignoreaw = false
		end
	end)
	ply.ignoreaw = true
end

//After the guy is unarrested, he'll shove out a gun for a split second, which the police can respond to.
//This prevents that by adding a ignore for a few seconds.
hook.Add("playerUnArrested", "AutoWanted_2", function(criminal, actor)
	SetPlayerIgnore(criminal)
end)

hook.Add("PlayerDeath", "AutoWanted_2", function(ply,wep,killer)
	//Check if someone is on his way to make this guy wanted, if, then we stop them!
	for k,v in pairs(player.GetAll()) do
		if v.pollingwantedreason then
			local crim = v.pollingwantedreason.criminal
			if crim == ply then
				//ABORT
				AbortWanted(v)
				GAMEMODE:notify(v, 0, 5, "The criminal died before you could report him!")
			end
		end
	end
	
	if ply.pollingwantedreason then //If he died, he shouldn't be able to continue with the wanted.
		AbortWanted(ply)
	end

	//In gamemode_functions, wanted is set to false without notifying everybody else. So we have to simulate same shit here.
	if IsValid(ply) and (ply ~= killer or ply.Slayed) and not ply:isArrested() then
		ply:CleanWantedReasons()
	end
end)

/*
Murder
*/
hook.Add("PlayerDeath", "AutoWanted", function( ply, wep, killer )
	if not AUTOWANTED_REASONS.Murder then return end
	
	if IsValid(killer) and killer:IsPlayer() and killer != ply and !ply:isCP() then
		if playerCanBeAutowanted(killer) then // If he's already a murder, we don't need to do stupid processing to check if he's a murderer.
			
			//First make sure we see the victim's pos
			local b,t = isPoliceWatching(ply:GetPos() + Vector(0,0,40))
			if b then
				//Then we gotta make sure we see the killer too. This is important because the killer might be far away (sniper).
				b,t = isListWatching(t, killer:GetPos() + Vector(0,0,40))
				if b then
					for k,v in pairs(t) do
						if v != ply and v != killer then //Make sure the cop isn't the killer or victim, lel
							if not killer:AddWantedReason("Убийство", v, true) then continue end
						end
						break
					end
				end
			end
		end
	end
end)

/*
Attempted murder, harm
*/
hook.Add("EntityTakeDamage", "AutoWanted", function( ent, dmginfo )
	if not AUTOWANTED_REASONS.Harm then return end
	
	local atkr = dmginfo:GetAttacker()
	
	if ent:IsPlayer() and IsValid(atkr) and atkr:IsPlayer() and ent != atkr and playerCanBeAutowanted(atkr) then
		local dmg = dmginfo:GetDamage()
		local hp = ent:Health()
		if dmg < hp then // If it's a killing shot, its a murder and not attempted, and thats not something we process here.
			local reason = "Ущерб здоровью"
			
			
			local b,t = isPoliceWatching(ent:GetPos() + Vector(0,0,40))
		
			if playerCanReportCrime(ent) then //If the victim is police, he should be able to report.
				b = true
				table.insert(t, ent)
			end
			
			if b then
				b,t = isListWatching(t, atkr:GetPos() + Vector(0,0,40))
				if b then
					for k,v in pairs(t) do
						if v != atkr then // Make sure the police isn't the killer.
							if not atkr:AddWantedReason("Ущерб здоровью", v, false) then continue end
						end
						break
					end
				end
			end
		end
	end
end)

/*
Unauthorized gun wielding
*/
//Returns true if this guy isn't allowed weapons
local function isHoldingUnauthWep(ply)
	if playerAllowedToHoldGuns(ply) then return false end
	if ply.ignoreaw then return false end
	if ply:getDarkRPVar("HasGunlicense") then return false end
	
	local weapon = ply:GetActiveWeapon()
	if not IsValid(weapon) then return false end
	if aw_legalweapons[string.lower(weapon:GetClass())] or not weapon:IsWeapon() then return false end
	
	return true
end
local function runUnauthCheck(ply)
	if not AUTOWANTED_REASONS.Unauthguns then return end
	
	if isHoldingUnauthWep(ply) and playerCanBeAutowanted(ply) then
		local b,t = isPoliceWatching(ply:GetPos() + Vector(0,0,40))
		if b then
			for k,v in pairs(t) do
				if v != ply then
					if not ply:AddWantedReason("Нелегальное оружие", v, false) then continue end
				end
				break
			end
		end
	end
end
hook.Add("PlayerSwitchWeapon", "AutoWanted", function(ply) runUnauthCheck(ply) end) // Check everytime someone switches weapon
timer.Create("AutoWantedCheckUnauthWeapons", 3, 0, function() for k,v in pairs(player.GetAll()) do runUnauthCheck(v) end end) // Check every third second

/*
Contraband
*/
local function runContrabandCheck()
	if not AUTOWANTED_REASONS.Contraband then return end
	
	for k,v in pairs(ents.GetAll()) do
		if v:IsPlayerHolding() and isContraband(v) then
			
			//We need to find who "ply" is here before we can continue.
			local ply = v.av_pkpply
			if IsValid(ply) and not ply.ignoreaw and not ply:HasWantedReason("Нелегальный предмет") and playerCanBeAutowanted(ply) then
				local b,t = isPoliceWatching(ply:GetPos() + Vector(0,0,40))
				if b then
					for k,v in pairs(t) do
						if v != ply then
							if not ply:AddWantedReason("Нелегальный предмет", v, false) then continue end
						end
						break
					end
				end
			end
		end
	end
end
timer.Create("AutoWantedCheckContraband", 3, 0, function() runContrabandCheck() end) // Check every third second


//Asscrap system to save what he picked up.
//This will probably not work in all cases, but its better than nothing.
hook.Add("AllowPlayerPickup", "AutoWanted", function(ply, ent)
	ply.av_pkpent = ent
	ent.av_pkpply = ply
end)
hook.Add("GravGunOnPickedUp", "AutoWanted", function(ply, ent)
	ply.av_pkpent = ent
	ent.av_pkpply = ply
end)
hook.Add("PhysgunPickup", "AutoWanted", function(ply, ent)
	ply.av_pkpent = ent
	ent.av_pkpply = ply
end)

/*
Jail-bailing
*/
hook.Add("playerUnArrested", "AutoWanted", function( ply, actor )
	if not AUTOWANTED_REASONS.Jailbailing then return end
	
	if IsValid(actor) and actor:IsPlayer() and not actor:isCP() then // Welp, a cop didn't unarrest him. Must be a criminal!
		if not actor:HasWantedReason("Jailbailing") and playerCanBeAutowanted(actor) then
			local b,t = isPoliceWatching(actor:GetPos() + Vector(0,0,40))
			if b then
				for k,v in pairs(t) do
					if v != ply then //Make sure the cop isn't the victim
						if not actor:AddWantedReason("Jailbailing", v, false) then continue end
					end
					break
				end
			end
		end
	end
end)

/*
Arrest-On-Sight
*/
local function RunAoSCheck(ply)
	if not AUTOWANTED_REASONS.ArrestOnSight then return end
	
	if playerHasIllegalJob(ply) then
		if not ply:HasWantedReason("Illegal Job") and playerCanBeAutowanted(ply) then
			local b,t = isPoliceWatching(ply:GetPos() + Vector(0,0,40))
			if b then
				for k,v in pairs(t) do
					if v != ply then //Make sure the cop isn't the victim
						if not ply:AddWantedReason("Illegal Job", v, false) then continue end
					end
					break
				end
			end
		end
	end
end
timer.Create("AutoWantedCheckAoS", 3, 0, function()
	for k,v in pairs(player.GetAll()) do 
		RunAoSCheck(v)
	end
end) // Check every third second

/*
Lockpicking, pickpocket, keypadcracking
*/
local function RunActivityCheck(ply)
	if not AUTOWANTED_REASONS.IllegalActivity then return end
	
	if playerCommitingIllegalActivity(ply) then
		if not ply:HasWantedReason("Illegal Activity") and playerCanBeAutowanted(ply) then
			local b,t = isPoliceWatching(ply:GetPos() + Vector(0,0,40))
			if b then
				for k,v in pairs(t) do
					if v != ply then //Make sure the cop isn't the victim
						if not ply:AddWantedReason("Illegal Activity", v, false) then continue end
					end
					break
				end
			end
		end
	end
end
timer.Create("AutoWantedCheckActivity", 3, 0, function()
	for k,v in pairs(player.GetAll()) do 
		RunActivityCheck(v)
	end
end) // Check every third second

hook.Add("onLockpickCompleted", "WantedLockpick", function( actor, success, ent )
	if IsValid(actor) and actor:IsPlayer() and not actor:isCP() then
		if playerCanBeAutowanted(actor) then
			local b,t = isPoliceWatching(actor:GetPos() + Vector(0,0,40))
			if b then
				for k,v in pairs(t) do
					if v != ply then
						if not actor:AddWantedReason("Взлом", v, true) then continue end
					end
					break
				end
			end
		end
	end
end)



