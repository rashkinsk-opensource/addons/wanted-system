
AUTOWANTED_REACTTIME = 10		// Сколько секунд держится сообщение
AUTOWANTED_RESPONSETIME = 1	// Через сколько секунд будет отправлено оповещение

local function ShowDocuments(answer, Ent, Initiator, Target)
    Initiator.DocumentsRequested = nil
	local wantedreason = Target:getDarkRPVar('wantedReason') or "Отсутствует"
    if tobool(answer) then
        DarkRP.notify(Initiator, 0, 4, "Данные о гражданском лице "..Target:Nick().." получены.")
        DarkRP.notify(Target, 0, 4, "Вы показали документы сотруднику "..Initiator:Nick())
		Target:SetNWBool("Wanted_Visible", true)
		if GetWantedLevel(Target) == 1 then
			if !timer.Exists("WantedShow"..Target:EntIndex()) then
				timer.Create("WantedShow"..Target:EntIndex(), WANTED_CONFIG.ShowWanted_1, 1, function() 
					Target:SetNWBool("Wanted_Visible", false)
					DarkRP.notify(Target, 0, 10, "Теперь полиция не видит ваш розыск, пока не проверит документы.")	
				end)
			end
        elseif GetWantedLevel(Target) == 2 then
			if !timer.Exists("WantedShow"..Target:EntIndex()) then
				timer.Create("WantedShow"..Target:EntIndex(), WANTED_CONFIG.ShowWanted_2, 1, function() 
					Target:SetNWBool("Wanted_Visible", false)
					DarkRP.notify(Target, 0, 10, "Теперь полиция не видит ваш розыск, пока не проверит документы.")	
				end)
			end
		elseif GetWantedLevel(Target) == 3 then
			Target:SetNWBool("Wanted_Visible", true)
			timer.Remove("WantedShow"..Target:EntIndex())
			DarkRP.notify(Initiator, 0, 4, "Гражданин "..Target:Nick().." имеет 3 уровень розыска и был пробит по базе.")
		end
		Initiator:ChatPrint( "-=ИНФОРМАЦИЯ=-\nИмя: "..Target:Nick().."\nУровень розыска: "..GetWantedLevel(Target).."\nПричина: "..wantedreason )
    else
        DarkRP.notify(Initiator, 1, 4, "Гражданин "..Target:Nick().." отказался показать вам документы")
		if GetWantedLevel(Target) == 2 then
			if !timer.Exists("WantedShow"..Target:EntIndex()) then
				Target:SetNWBool("Wanted_Visible", true)
				DarkRP.notify(Initiator, 0, 4, "Гражданин "..Target:Nick().." имеет 2 уровень розыска и был пробит по базе.")
				timer.Create("WantedShow"..Target:EntIndex(), WANTED_CONFIG.ShowWanted_2, 1, function() 
					Target:SetNWBool("Wanted_Visible", false)
					DarkRP.notify(Target, 0, 10, "Теперь полиция не видит ваш розыск, пока не проверит документы.")	
				end)
			else
				Target:SetNWBool("Wanted_Visible", true)
				DarkRP.notify(Initiator, 0, 4, "Гражданин "..Target:Nick().." имеет 2 уровень розыска и был пробит по базе.")
			end
		elseif GetWantedLevel(Target) == 3 then
			Target:SetNWBool("Wanted_Visible", true)
			timer.Remove("WantedShow"..Target:EntIndex())
			DarkRP.notify(Initiator, 0, 4, "Гражданин "..Target:Nick().." имеет 3 уровень розыска и был пробит по базе. Применить оружие.")
			DarkRP.notify(Target, 0, 10, "Вы не показали докумены, но вас пробили по ориентировке и вы были опознаны.")	
		end
    end
end

local function RequestDocuments(ply)
   
	if ply.DocumentsRequested or !ply:isCP() then return end
    local LookingAt = ply:GetEyeTrace().Entity

    if not IsValid(LookingAt) or not LookingAt:IsPlayer() or LookingAt:GetPos():Distance(ply:GetPos()) > 100 then
        DarkRP.notify(ply, 1, 4, "У воздуха нет документов. Смотрите на игрока.")
        return ""
    end

    ply.DocumentsRequested = true
    DarkRP.notify(ply, 3, 4, "Вы запросили документы у "..LookingAt:Nick())
    DarkRP.createQuestion("Показать документы\nсотруднику "..ply:Nick(), "Documents" .. ply:EntIndex(), LookingAt, 20, ShowDocuments, ply, LookingAt)
    return ""
end

DarkRP.declareChatCommand{
        command = "papers",
        description = "Create a billboard holding an advertisement.",
        delay = 1.5
    }

timer.Simple(5, function() 
if SERVER then
	DarkRP.defineChatCommand("papers", RequestDocuments) 
end
end)
